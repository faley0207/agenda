<html>
    <head>
        <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css"
        <link rel="stylesheet" type="text/css" href="lib/DataTables-1.10.15/media/css/jquery.dataTables.css">
        <script type="text/javascript" src="lib/jQuery/jquery-3.2.1.js"></script>
        <script type="text/javascript" src="lib/DataTables-1.10.15/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="lib/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="js/usuarios.js"></script>
    </head>
    <body>
        <form method="POST" action="index">
            <div class="container">
                <br>
                <div class="row">
                    <label class="col-md-6">Identificacion</label>
                    <input class="col-md-6" type="number" id="identificacionUsu"/>
                </div>   
                <br>
               <div class="row">
                    <label class="col-md-6" >Nombres</label>
                    <input class="col-md-6" type="text" id="nombresUsu" size="50"/>
                </div> 
                <br>
                <div class="row">
                    <label class="col-md-6">correo</label>
                    <input class="col-md-6" type="text" id="apellidosUsu"/>
                </div> 
                <br>
                <div class="row">
                    <label class="col-md-6">edad</label>
                    <input class="col-md-6" type="text" id="fNacimientoUsu"/>
                </div>  
                <br>
                <div class="row">
                    <label class="col-md-6">Genero</label>
                    <select  class="col-md-6" id="genero">
                        <option id="0">Femenino</option>
                        <option id="1">Masculino</option>
                    </select>
                </div>
                <br>
                <div class="row">
                <input type="button" id="createUsuario" value="Crear Usuario" class="btn-success col-md-4 col-lg-offset-4"/>
                    
                </div>
            </div>

        </form>
            
    </body>
    
</html>

